import { DriverManager, Account, Exception } from 'auth-br'
import { Db, Collection } from 'mongodb'

/*
 * Extender a validação
 */
export interface Schema {
    properties: any;
    required: [any];
}

/**
 * Opções do Driver
 */
export interface DriverOptions {
    account_name: string;
    database: Db;
    schema?: Schema;
    account: Collection<any>;
}

class MongoDBDriver extends DriverManager<DriverOptions> {
    private options: DriverOptions

    /**
     * Registro de usuário
     *
     * @param account
     * @param extra
     */
    async register (account: Account, extra: any): Promise<boolean> {
      try {
        delete account.id
        const result = Object.assign(extra, account)
        const value = await this.options.account.insertOne(result)
        return value.insertedCount === 1
      } catch (error) {
        return false
      }
    }

    /**
     * Busca o usuário com base no email
     *
     * @param email Dado do usuário filtrado.
     */
    async find (email: string): Promise<Account | null> {
      try {
        const account = await this.options.account.findOne({ email })
        if (account == null) return null
        account.id = account._id
        return account
      } catch (error) {
        return null
      }
    }

    /**
     * Inicializa o schema
     */
    async initialize (): Promise<void> {
      const options = {
        validator: {
          $jsonSchema: {
            bsonType: 'object',
            properties: {
              name: {
                bsonType: 'string',
                maxLength: 120
              },
              email: {
                bsonType: 'string',
                maxLength: 256
              },
              password: {
                bsonType: 'string',
                maxLength: 60
              }
            },
            required: ['name', 'email', 'password']
          }
        }
      }
      //
      // Schema definida pelo usuário
      //
      if (this.options.schema) {
        Object.assign(options.validator.$jsonSchema.properties, this.options.schema.properties)
        this.options.schema.required.forEach(i => options.validator.$jsonSchema.required.push(i))
      }
      const collection = await this.options.database.createCollection(this.options.account_name, options)
      await collection.createIndex('email', { unique: true })
    }

    /**
     * Valida as opções
     *
     * @param {DriverOptions} options Valores recebidos do plugin
     */
    async validate (options: DriverOptions): Promise<void> {
      if (options.account_name == null) {
        throw new Exception('account == null')
      }
      if (typeof options.account_name !== 'string') {
        throw new Exception('account_name !== typeof "string"')
      }
      this.options = {
        account_name: options.account_name,
        database: options.database,
        schema: options.schema,
        get account () {
          return this.database.collection(this.account_name)
        }
      }
    }
}
export default MongoDBDriver
