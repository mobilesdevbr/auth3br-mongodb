"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const auth_br_1 = require("auth-br");
class MongoDBDriver extends auth_br_1.DriverManager {
    async register(account, extra) {
        try {
            delete account.id;
            const result = Object.assign(extra, account);
            const value = await this.options.account.insertOne(result);
            return value.insertedCount === 1;
        }
        catch (error) {
            return false;
        }
    }
    async find(email) {
        try {
            const account = await this.options.account.findOne({ email });
            if (account == null)
                return null;
            account.id = account._id;
            return account;
        }
        catch (error) {
            return null;
        }
    }
    async initialize() {
        const options = {
            validator: {
                $jsonSchema: {
                    bsonType: 'object',
                    properties: {
                        name: {
                            bsonType: 'string',
                            maxLength: 120
                        },
                        email: {
                            bsonType: 'string',
                            maxLength: 256
                        },
                        password: {
                            bsonType: 'string',
                            maxLength: 60
                        }
                    },
                    required: ['name', 'email', 'password']
                }
            }
        };
        if (this.options.schema) {
            Object.assign(options.validator.$jsonSchema.properties, this.options.schema.properties);
            this.options.schema.required.forEach(i => options.validator.$jsonSchema.required.push(i));
        }
        const collection = await this.options.database.createCollection(this.options.account_name, options);
        await collection.createIndex('email', { unique: true });
    }
    async validate(options) {
        if (options.account_name == null) {
            throw new auth_br_1.Exception('account == null');
        }
        if (typeof options.account_name !== 'string') {
            throw new auth_br_1.Exception('account_name !== typeof "string"');
        }
        this.options = {
            account_name: options.account_name,
            database: options.database,
            schema: options.schema,
            get account() {
                return this.database.collection(this.account_name);
            }
        };
    }
}
exports.default = MongoDBDriver;
