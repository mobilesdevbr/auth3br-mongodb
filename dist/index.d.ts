import { DriverManager, Account } from 'auth-br';
import { Db, Collection } from 'mongodb';
export interface Schema {
    properties: any;
    required: [any];
}
export interface DriverOptions {
    account_name: string;
    database: Db;
    schema?: Schema;
    account: Collection<any>;
}
declare class MongoDBDriver extends DriverManager<DriverOptions> {
    private options;
    register(account: Account, extra: any): Promise<boolean>;
    find(email: string): Promise<Account | null>;
    initialize(): Promise<void>;
    validate(options: DriverOptions): Promise<void>;
}
export default MongoDBDriver;
